const express = require('express');
const cors = require('cors');
const app = express();
var path = require('path');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

//SETTINGS
app.set('port', process.env.PORT || 4000);

//configuracion de swagger
const swaggerOptions = {
    swaggerDefinition:{
        info: {
            title: 'Backend Fiscalías, Desarrollador: Mauro Jutzutz',
            description: 'Prueba tecnica de backend de fiscalias, herramientas, node, express, swagger, mongoDB',
            contact: {
                name: 'Mauro Augusto Jutzutz Sepet'
            },
            servers: ["http://localhost:4000"]
        }
    },
    //[".routes/*.js"]
    apis: ["src/app.js"]
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));


//midlewares
app.use(cors());
app.use(express.json());
//renderiza frontend estatico de react

/**
 * @swagger
 * /:
 *  get:
 *    description: Backend de desarrollador 
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.get('/', (req, res) => {
  res.json({valid: true,
            msg: "Bienvenido Backend de Fiscalías",
            author: "Mauro Jutzutz"})
});

//routes

/**
 * @swagger
 * /api/fiscalias:
 *  get:
 *    summary: "Listado de fiscalías registradas"
 *    description: listado de fiscalias 
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.use('/api/fiscalias/{id}', require('./routes/fiscalias'));

/**
 * @swagger
 * /api/fiscalias/{id}:
 *  put:
 *    summary: "editar fiscalía"
 *    consumes: 
 *      - application/json
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: any
 *      - in: body
 *        name: user
 *        description: user
 *        schema: 
 *          type: object
 *          properties:
 *            fiscalia:
 *              type: string
 *            municipio:
 *              type: string
 *            direccion:
 *              type: string
 *            telefono:
 *              type: integer
 *            date:
 *              type: string
 *            author:
 *              type: string
 *            content:
 *              type: string
 *      
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.use('/api/fiscalias', require('./routes/fiscalias'));

/**
 * @swagger
 * /api/fiscalias/{id}:
 *  get:
 *    summary: "Buscar fiscalía"
 *    description: realiza una busqueda especifica de una fiscalia mediante su id 
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: any
 *        required: true 
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.use('/api/fiscalias', require('./routes/fiscalias'));

/**
 * @swagger
 * /api/fiscalias/{id}:
 *  delete:
 *    description: listado de fiscalias 
 *    summary: eliminar fiscalia
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: any
 *        required: true 
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.use('/api/fiscalias', require('./routes/fiscalias'));

/**
 * @swagger
 * /api/users:
 *  get:
 *    description: listado de fiscalias 
 *    responses: 
 *      '200':
 *        description: peticion exitosa
 */
app.use('/api/users', require('./routes/users'));

module.exports = app;